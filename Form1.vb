Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tbTickerMessage As System.Windows.Forms.TextBox
    Friend WithEvents tbDisplayPeriod As System.Windows.Forms.TextBox
    Friend WithEvents tbAuthor As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.tbTickerMessage = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.tbDisplayPeriod = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.tbAuthor = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbTickerMessage
        '
        Me.tbTickerMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbTickerMessage.Location = New System.Drawing.Point(3, 16)
        Me.tbTickerMessage.Name = "tbTickerMessage"
        Me.tbTickerMessage.Size = New System.Drawing.Size(474, 20)
        Me.tbTickerMessage.TabIndex = 0
        Me.tbTickerMessage.Text = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.tbTickerMessage})
        Me.GroupBox1.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(480, 40)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ticker Melding"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.AddRange(New System.Windows.Forms.Control() {Me.tbDisplayPeriod})
        Me.GroupBox2.Location = New System.Drawing.Point(8, 72)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 40)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Visningsperiode (minutter)"
        '
        'tbDisplayPeriod
        '
        Me.tbDisplayPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbDisplayPeriod.Location = New System.Drawing.Point(3, 16)
        Me.tbDisplayPeriod.Name = "tbDisplayPeriod"
        Me.tbDisplayPeriod.Size = New System.Drawing.Size(194, 20)
        Me.tbDisplayPeriod.TabIndex = 1
        Me.tbDisplayPeriod.Text = ""
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.AddRange(New System.Windows.Forms.Control() {Me.tbAuthor})
        Me.GroupBox3.Location = New System.Drawing.Point(224, 72)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(264, 40)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Forfatter"
        '
        'tbAuthor
        '
        Me.tbAuthor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbAuthor.Location = New System.Drawing.Point(3, 16)
        Me.tbAuthor.Name = "tbAuthor"
        Me.tbAuthor.Size = New System.Drawing.Size(258, 20)
        Me.tbAuthor.TabIndex = 1
        Me.tbAuthor.Text = ""
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(16, 128)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(144, 24)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "Lagre"
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(176, 128)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(144, 24)
        Me.btnNew.TabIndex = 7
        Me.btnNew.Text = "Ny Melding"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(336, 128)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(144, 24)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Avslutt"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(496, 165)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnExit, Me.btnNew, Me.btnSave, Me.GroupBox3, Me.GroupBox2, Me.GroupBox1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Ny ticker melding"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If tbTickerMessage.Text = "" Then
            MsgBox("Ticker meldingen er tom")
            Exit Sub
        End If

        If tbDisplayPeriod.Text = "" Then
            MsgBox("Visningsperiode er ikke angitt")
            Exit Sub
        End If

        Dim intMinutes As Integer
        Try
            intMinutes = CInt(tbDisplayPeriod.Text)
        Catch
            MsgBox("Visningsperiode er ikke angitt")
            Exit Sub
        End Try

        If tbAuthor.Text = "" Then
            MsgBox("Forfatter m� oppgis")
            Exit Sub
        End If

        Dim myConnection As SqlConnection
        Dim mySqlCommand As SqlCommand

        'all checks complete, starting procedure for updating article in database.
        myConnection = New SqlConnection(AppSettings("SqlConnectionString"))
        mySqlCommand = New SqlCommand()

        myConnection.Open()

        mySqlCommand.CommandText = "[newTickerMessage]"
        mySqlCommand.CommandType = System.Data.CommandType.StoredProcedure
        mySqlCommand.Connection = myConnection
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@what", System.Data.SqlDbType.VarChar, 255))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@messageorigin", System.Data.SqlDbType.VarChar, 63))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@priority", System.Data.SqlDbType.TinyInt, 1))
        mySqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@minutestoshow", System.Data.SqlDbType.Int, 4))

        With mySqlCommand
            .Parameters("@what").Value = "/*M:\y" & tbTickerMessage.Text & "*/"
            .Parameters("@messageorigin").Value = tbAuthor.Text
            .Parameters("@priority").Value = 10
            .Parameters("@minutestoshow").Value = CInt(tbDisplayPeriod.Text)
            Try
                .ExecuteNonQuery()
            Catch err As Exception
                MsgBox(err.Message & vbCrLf & err.StackTrace)
            End Try

        End With

        myConnection.Close()

        tbTickerMessage.Enabled = False
        tbDisplayPeriod.Enabled = False
        tbAuthor.Enabled = False

    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        tbTickerMessage.Clear()
        tbDisplayPeriod.Clear()
        tbAuthor.Clear()
        tbTickerMessage.Enabled = True
        tbDisplayPeriod.Enabled = True
        tbAuthor.Enabled = True
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Application.Exit()
    End Sub
End Class
